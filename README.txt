
DESCRIPTION
-----------

Selector lets you select a Features component to be exported to a feature
right in the place in Drupal UI where a settings element for it resides. It provides
a widget in that place near the element itself, which is giving you also information
about the particular component and its presence (status) in available features.

At the moment it provides support for all core modules and these modules in addition:

- Administration menu
- Devel
- Poormanscron

You can also add support for your own module by using the API to tell where in which
form or a page an exportable component is located.
