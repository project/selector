<?php

/**
 * @file
 * Hooks provided by Selector.
 */

/**
 * @addtogroup hooks
 * @{
 * Hooks that can be implemented by other modules in order to use the Selector API.
 */

/**
 * Describe relations of exportable components and Drupal UI elements.
 *
 * Using this hook you can tell where exportable components defined by your module
 * are located in Drupal UI. First you define which form element holds the component,
 * then you can add path conditions where it is necessary. You can also add more
 * components to one form element, if you want them to be dynamically filtered by
 * path conditions. The idea is to have only one component on a form element at
 * the same time, and the implementation makes sure of that.
 */
function hook_selector_map() {
  $items = array();

  // Simple example definition.
  // admin/settings/example
  $items['form']['example_admin_settings']['example_element']['#features'][] = array(
    'type' => 'example_type',
    'component' => 'example_component',
  );

  // More complex example with the use of path conditions.
  // admin/build/example/%type
  $types = array('foo', 'bar');
  foreach ($types as $type) {
    $items['form']['example_form']['example_element']['#features'][] = array(
      'type' => 'example_type',
      'component' => 'example_component_' . $type,
    );
    $items['page']['admin/build/example/' . $type]['#features'][] = array(
      'type' => 'example_type',
      'component' => 'example_component_' . $type,
    );
  }

  return $items;
}

/**
 * Alter the Drupal UI to component map.
 *
 * You can use this hook where it makes sense to alter the map defined by other
 * modules previously. For example if your module changes an existing settings form.
 */
function hook_selector_map_alter(&$items) {
  // admin/settings/example
  unset($items['form']['example_admin_settings']['example_element']['#features'][0]);
  $items['form']['example_admin_settings']['another_example_element']['#features'][] = array(
    'type' => 'example_type',
    'component' => 'example_component',
  );
}

/**
 * @} End of "addtogroup hooks".
 */

/**
 * @addtogroup patterns
 * @{
 * Patterns recommended for use by other modules requiring customization which goes
 * beyond the capabilities provided by Selector API.
 */

/**
 * Implements hook_form_alter().
 *
 * If you don't like how Selector displays the widget in a particular place
 * in the Drupal UI, or more precisely you need to append it differently, you can
 * do it by your own just by copying and modifying this pattern.
 */
function mymodule_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'example_admin_settings') {
    if (isset($form['example_element'])) {
      $form['example_element']['#prefix'] = @$form['example_element']['#prefix'] . selector_widget('example_type', 'example_element');
    }
  }
}

/**
 * @} End of "addtogroup patterns".
 */
