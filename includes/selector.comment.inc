<?php

/**
 * @file
 * A file containing the Drupal UI to Feature's components map for core Comment module.
 */

/**
 * Implements hook_selector_map().
 */
function comment_selector_map() {
  $items = array();

  // Found in comment.module
  // Extending Node module.
  // admin/content/node-type/%type
  foreach (node_get_types('types', NULL, TRUE) as $type) {
    $type_url_str = str_replace('_', '-', $type->type);
    $items['form']['node_type_form']['comment']['comment']['#features'][] = array(
      'type' => 'variable',
      'component' => 'comment_'. $type->type,
    );
    $items['page']['admin/content/node-type/' . $type_url_str]['#features'][] = array(
      'type' => 'variable',
      'component' => 'comment_' . $type->type,
    );
    $items['form']['node_type_form']['comment']['comment_default_mode']['#features'][] = array(
      'type' => 'variable',
      'component' => 'comment_default_mode_'. $type->type,
    );
    $items['page']['admin/content/node-type/' . $type_url_str]['#features'][] = array(
      'type' => 'variable',
      'component' => 'comment_default_mode_' . $type->type,
    );
    $items['form']['node_type_form']['comment']['comment_default_order']['#features'][] = array(
      'type' => 'variable',
      'component' => 'comment_default_order_'. $type->type,
    );
    $items['page']['admin/content/node-type/' . $type_url_str]['#features'][] = array(
      'type' => 'variable',
      'component' => 'comment_default_order_' . $type->type,
    );
    $items['form']['node_type_form']['comment']['comment_default_per_page']['#features'][] = array(
      'type' => 'variable',
      'component' => 'comment_default_per_page_'. $type->type,
    );
    $items['page']['admin/content/node-type/' . $type_url_str]['#features'][] = array(
      'type' => 'variable',
      'component' => 'comment_default_per_page_' . $type->type,
    );
    $items['form']['node_type_form']['comment']['comment_controls']['#features'][] = array(
      'type' => 'variable',
      'component' => 'comment_controls_'. $type->type,
    );
    $items['page']['admin/content/node-type/' . $type_url_str]['#features'][] = array(
      'type' => 'variable',
      'component' => 'comment_controls_' . $type->type,
    );
    $items['form']['node_type_form']['comment']['comment_anonymous']['#features'][] = array(
      'type' => 'variable',
      'component' => 'comment_anonymous_'. $type->type,
    );
    $items['page']['admin/content/node-type/' . $type_url_str]['#features'][] = array(
      'type' => 'variable',
      'component' => 'comment_anonymous_' . $type->type,
    );
    $items['form']['node_type_form']['comment']['comment_subject_field']['#features'][] = array(
      'type' => 'variable',
      'component' => 'comment_subject_field_'. $type->type,
    );
    $items['page']['admin/content/node-type/' . $type_url_str]['#features'][] = array(
      'type' => 'variable',
      'component' => 'comment_subject_field_' . $type->type,
    );
    $items['form']['node_type_form']['comment']['comment_preview']['#features'][] = array(
      'type' => 'variable',
      'component' => 'comment_preview_'. $type->type,
    );
    $items['page']['admin/content/node-type/' . $type_url_str]['#features'][] = array(
      'type' => 'variable',
      'component' => 'comment_preview_' . $type->type,
    );
    $items['form']['node_type_form']['comment']['comment_form_location']['#features'][] = array(
      'type' => 'variable',
      'component' => 'comment_form_location_'. $type->type,
    );
    $items['page']['admin/content/node-type/' . $type_url_str]['#features'][] = array(
      'type' => 'variable',
      'component' => 'comment_form_location_' . $type->type,
    );
  }

  return $items;
}
