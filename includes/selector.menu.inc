<?php

/**
 * @file
 * A file containing the Drupal UI to Feature's components map for core Menu module.
 */

/**
 * Implements hook_selector_map().
 */
function menu_selector_map() {
  $items = array();

  // Found in menu.admin.inc
  // admin/build/menu/settings
  $items['form']['menu_configure']['menu_default_node_menu']['#features'][] = array(
    'type' => 'variable',
    'component' => 'menu_default_node_menu',
  );
  $items['form']['menu_configure']['menu_primary_links_source']['#features'][] = array(
    'type' => 'variable',
    'component' => 'menu_primary_links_source',
  );
  $items['form']['menu_configure']['menu_secondary_links_source']['#features'][] = array(
    'type' => 'variable',
    'component' => 'menu_secondary_links_source',
  );

  return $items;
}
