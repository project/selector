<?php

/**
 * @file
 * A file containing the Drupal UI to Feature's components map for core Update status module.
 */

/**
 * Implements hook_selector_map().
 */
function update_selector_map() {
  $items = array();

  // Found in update.settings.inc
  // admin/reports/updates/settings
  $items['form']['update_settings']['update_notify_emails']['#features'][] = array(
    'type' => 'variable',
    'component' => 'update_notify_emails',
  );
  $items['form']['update_settings']['update_check_frequency']['#features'][] = array(
    'type' => 'variable',
    'component' => 'update_check_frequency',
  );
  $items['form']['update_settings']['update_notification_threshold']['#features'][] = array(
    'type' => 'variable',
    'component' => 'update_notification_threshold',
  );

  return $items;
}
