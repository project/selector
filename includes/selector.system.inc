<?php

/**
 * @file
 * A file containing the Drupal UI to Feature's components map for core System module.
 */

/**
 * Implements hook_selector_map().
 */
function system_selector_map() {
  $items = array();

  // Found in system.module
  // Implementing Block module.
  // admin/build/block/configure/system/0
  $items['form']['block_admin_configure']['block_settings']['wrapper']['color']['#features'][] = array(
    'type' => 'variable',
    'component' => 'drupal_badge_color',
  );
  $items['form']['block_admin_configure']['block_settings']['wrapper']['size']['#features'][] = array(
    'type' => 'variable',
    'component' => 'drupal_badge_size',
  );

  // Found in system.admin.inc
  // admin/settings/admin
  $items['form']['system_admin_theme_settings']['admin_theme']['#features'][] = array(
    'type' => 'variable',
    'component' => 'admin_theme',
  );
  $items['form']['system_admin_theme_settings']['node_admin_theme']['#features'][] = array(
    'type' => 'variable',
    'component' => 'node_admin_theme',
  );

  // admin/build/themes
  $items['form']['system_themes_form']['theme_default']['#features'][] = array(
    'type' => 'variable',
    'component' => 'theme_default',
  );

  // admin/build/themes/settings
  $items['form']['system_theme_settings']['theme_settings']['#features'][] = array(
    'type' => 'variable',
    'component' => 'theme_settings',
  );
  $items['page']['admin/build/themes/settings']['#features'][] = array(
    'type' => 'variable',
    'component' => 'theme_settings',
  );

  // admin/build/themes/settings/%theme
  foreach (list_themes() as $theme) {
    $items['form']['system_theme_settings']['theme_settings']['#features'][] = array(
      'type' => 'variable',
      'component' => 'theme_' . $theme->name . '_settings',
    );
    $items['page']['admin/build/themes/settings/' . $theme->name]['#features'][] = array(
      'type' => 'variable',
      'component' => 'theme_' . $theme->name . '_settings',
    );
  }

  // admin/settings/site-information
  $items['form']['system_site_information_settings']['site_name']['#features'][] = array(
    'type' => 'variable',
    'component' => 'site_name',
  );
  $items['form']['system_site_information_settings']['site_mail']['#features'][] = array(
    'type' => 'variable',
    'component' => 'site_mail',
  );
  $items['form']['system_site_information_settings']['site_slogan']['#features'][] = array(
    'type' => 'variable',
    'component' => 'site_slogan',
  );
  $items['form']['system_site_information_settings']['site_mission']['#features'][] = array(
    'type' => 'variable',
    'component' => 'site_mission',
  );
  $items['form']['system_site_information_settings']['site_footer']['#features'][] = array(
    'type' => 'variable',
    'component' => 'site_footer',
  );
  $items['form']['system_site_information_settings']['anonymous']['#features'][] = array(
    'type' => 'variable',
    'component' => 'anonymous',
  );
  $items['form']['system_site_information_settings']['site_frontpage']['#features'][] = array(
    'type' => 'variable',
    'component' => 'site_frontpage',
  );

  // admin/settings/error-reporting
  $items['form']['system_error_reporting_settings']['site_403']['#features'][] = array(
    'type' => 'variable',
    'component' => 'site_403',
  );
  $items['form']['system_error_reporting_settings']['site_404']['#features'][] = array(
    'type' => 'variable',
    'component' => 'site_404',
  );
  $items['form']['system_error_reporting_settings']['error_level']['#features'][] = array(
    'type' => 'variable',
    'component' => 'error_level',
  );

  // admin/settings/performance
  $items['form']['system_performance_settings']['page_cache']['cache']['#features'][] = array(
    'type' => 'variable',
    'component' => 'cache',
  );
  $items['form']['system_performance_settings']['page_cache']['cache_lifetime']['#features'][] = array(
    'type' => 'variable',
    'component' => 'cache_lifetime',
  );
  $items['form']['system_performance_settings']['page_cache']['page_compression']['#features'][] = array(
    'type' => 'variable',
    'component' => 'page_compression',
  );
  $items['form']['system_performance_settings']['block_cache']['block_cache']['#features'][] = array(
    'type' => 'variable',
    'component' => 'block_cache',
  );
  $items['form']['system_performance_settings']['bandwidth_optimizations']['preprocess_css']['#features'][] = array(
    'type' => 'variable',
    'component' => 'preprocess_css',
  );
  $items['form']['system_performance_settings']['bandwidth_optimizations']['preprocess_js']['#features'][] = array(
    'type' => 'variable',
    'component' => 'preprocess_js',
  );

  // admin/settings/file-system
  $items['form']['system_file_system_settings']['file_directory_path']['#features'][] = array(
    'type' => 'variable',
    'component' => 'file_directory_path',
  );
  $items['form']['system_file_system_settings']['file_directory_temp']['#features'][] = array(
    'type' => 'variable',
    'component' => 'file_directory_temp',
  );
  $items['form']['system_file_system_settings']['file_downloads']['#features'][] = array(
    'type' => 'variable',
    'component' => 'file_downloads',
  );

  // admin/settings/image-toolkit
  $items['form']['system_image_toolkit_settings']['image_toolkit']['#features'][] = array(
    'type' => 'variable',
    'component' => 'image_toolkit',
  );
  // Found in /includes/image.gd.inc
  $items['form']['system_image_toolkit_settings']['image_toolkit_settings']['image_jpeg_quality']['#features'][] = array(
    'type' => 'variable',
    'component' => 'image_jpeg_quality',
  );

  // admin/content/rss-publishing
  $items['form']['system_rss_feeds_settings']['feed_default_items']['#features'][] = array(
    'type' => 'variable',
    'component' => 'feed_default_items',
  );
  $items['form']['system_rss_feeds_settings']['feed_item_length']['#features'][] = array(
    'type' => 'variable',
    'component' => 'feed_item_length',
  );

  // admin/settings/date-time
  $items['form']['system_date_time_settings']['locale']['date_default_timezone']['#features'][] = array(
    'type' => 'variable',
    'component' => 'date_default_timezone',
  );
  $items['form']['system_date_time_settings']['locale']['configurable_timezones']['#features'][] = array(
    'type' => 'variable',
    'component' => 'configurable_timezones',
  );
  $items['form']['system_date_time_settings']['locale']['date_first_day']['#features'][] = array(
    'type' => 'variable',
    'component' => 'date_first_day',
  );
  $items['form']['system_date_time_settings']['date_formats']['date_format_short']['#features'][] = array(
    'type' => 'variable',
    'component' => 'date_format_short',
  );
  $items['form']['system_date_time_settings']['date_formats']['date_format_short_custom']['#features'][] = array(
    'type' => 'variable',
    'component' => 'date_format_short_custom',
  );
  $items['form']['system_date_time_settings']['date_formats']['date_format_medium']['#features'][] = array(
    'type' => 'variable',
    'component' => 'date_format_medium',
  );
  $items['form']['system_date_time_settings']['date_formats']['date_format_medium_custom']['#features'][] = array(
    'type' => 'variable',
    'component' => 'date_format_medium_custom',
  );
  $items['form']['system_date_time_settings']['date_formats']['date_format_long']['#features'][] = array(
    'type' => 'variable',
    'component' => 'date_format_long',
  );
  $items['form']['system_date_time_settings']['date_formats']['date_format_long_custom']['#features'][] = array(
    'type' => 'variable',
    'component' => 'date_format_long_custom',
  );

  // admin/settings/site-maintenance
  $items['form']['system_site_maintenance_settings']['site_offline']['#features'][] = array(
    'type' => 'variable',
    'component' => 'site_offline',
  );
  $items['form']['system_site_maintenance_settings']['site_offline_message']['#features'][] = array(
    'type' => 'variable',
    'component' => 'site_offline_message',
  );

  // admin/settings/clean-urls
  $items['form']['system_clean_url_settings']['clean_url']['#features'][] = array(
    'type' => 'variable',
    'component' => 'clean_url',
  );

  return $items;
}
