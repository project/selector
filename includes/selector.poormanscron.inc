<?php

/**
 * @file
 * A file containing the Drupal UI to Feature's components map for Poormanscron module.
 */

/**
 * Implements hook_selector_map().
 */
function poormanscron_selector_map() {
  $items = array();

  // Found in poormanscron.module
  // admin/settings/site-information
  $items['form']['system_site_information_settings']['cron_safe_threshold']['#features'][] = array(
    'type' => 'variable',
    'component' => 'cron_safe_threshold',
  );

  return $items;
}
