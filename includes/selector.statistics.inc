<?php

/**
 * @file
 * A file containing the Drupal UI to Feature's components map for core Statistics module.
 */

/**
 * Implements hook_selector_map().
 */
function statistics_selector_map() {
  $items = array();

  // Found in statistics.module
  // Implementing Block module.
  // admin/build/block/configure/statistics/0
  $items['form']['block_admin_configure']['block_settings']['statistics_block_top_day_num']['#features'][] = array(
    'type' => 'variable',
    'component' => 'statistics_block_top_day_num',
  );
  $items['form']['block_admin_configure']['block_settings']['statistics_block_top_all_num']['#features'][] = array(
    'type' => 'variable',
    'component' => 'statistics_block_top_all_num',
  );
  $items['form']['block_admin_configure']['block_settings']['statistics_block_top_last_num']['#features'][] = array(
    'type' => 'variable',
    'component' => 'statistics_block_top_last_num',
  );

  // Found in statistics.admin.inc
  // admin/reports/settings
  $items['form']['statistics_access_logging_settings']['access']['statistics_enable_access_log']['#features'][] = array(
    'type' => 'variable',
    'component' => 'statistics_enable_access_log',
  );
  $items['form']['statistics_access_logging_settings']['access']['statistics_flush_accesslog_timer']['#features'][] = array(
    'type' => 'variable',
    'component' => 'statistics_flush_accesslog_timer',
  );
  $items['form']['statistics_access_logging_settings']['content']['statistics_count_content_views']['#features'][] = array(
    'type' => 'variable',
    'component' => 'statistics_count_content_views',
  );

  return $items;
}
