<?php

/**
 * @file
 * A file containing the Drupal UI to Feature's components map for core Filter module.
 */

/**
 * Implements hook_selector_map().
 */
function filter_selector_map() {
  $items = array();

  // Found in filter.module
  // admin/settings/filters/%format/configure
  foreach (filter_formats() as $format) {
    $items['form']['filter_admin_configure']['filter_html']['filter_html_' . $format->format]['#features'][] = array(
      'type' => 'variable',
      'component' => 'filter_html_' . $format->format,
    );
    $items['form']['filter_admin_configure']['filter_html']['allowed_html_' . $format->format]['#features'][] = array(
      'type' => 'variable',
      'component' => 'allowed_html_' . $format->format,
    );
    $items['form']['filter_admin_configure']['filter_html']['filter_html_help_' . $format->format]['#features'][] = array(
      'type' => 'variable',
      'component' => 'filter_html_help_' . $format->format,
    );
    $items['form']['filter_admin_configure']['filter_html']['filter_html_nofollow_' . $format->format]['#features'][] = array(
      'type' => 'variable',
      'component' => 'filter_html_nofollow_' . $format->format,
    );
    $items['form']['filter_admin_configure']['filter_urlfilter']['filter_url_length_' . $format->format]['#features'][] = array(
      'type' => 'variable',
      'component' => 'filter_url_length_' . $format->format,
    );
  }

  // Found in filter.admin.inc
  // admin/settings/filters
  $items['form']['filter_admin_overview']['default']['#features'][] = array(
    'type' => 'variable',
    'component' => 'filter_default_format',
  );

  return $items;
}
