<?php

/**
 * @file
 * A file containing the Drupal UI to Feature's components map for core Forum module.
 */

/**
 * Implements hook_selector_map().
 */
function forum_selector_map() {
  $items = array();

  // Found in forum.module
  // Implementing Block module.
  // admin/build/block/configure/forum/0
  $items['form']['block_admin_configure']['block_settings']['forum_block_num_0']['#features'][] = array(
    'type' => 'variable',
    'component' => 'forum_block_num_0',
  );
  // admin/build/block/configure/forum/1
  $items['form']['block_admin_configure']['block_settings']['forum_block_num_1']['#features'][] = array(
    'type' => 'variable',
    'component' => 'forum_block_num_1',
  );

  // Found in forum.admin.inc
  // admin/content/forum/settings
  $items['form']['forum_admin_settings']['forum_hot_topic']['#features'][] = array(
    'type' => 'variable',
    'component' => 'forum_hot_topic',
  );
  $items['form']['forum_admin_settings']['forum_per_page']['#features'][] = array(
    'type' => 'variable',
    'component' => 'forum_per_page',
  );
  $items['form']['forum_admin_settings']['forum_order']['#features'][] = array(
    'type' => 'variable',
    'component' => 'forum_order',
  );

  return $items;
}
