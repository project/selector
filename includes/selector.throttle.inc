<?php

/**
 * @file
 * A file containing the Drupal UI to Feature's components map for core Throttle module.
 */

/**
 * Implements hook_selector_map().
 */
function throttle_selector_map() {
  $items = array();

  // Found in throttle.admin.inc
  // admin/content/throttle/settings
  $items['form']['throttle_admin_settings']['throttle_anonymous']['#features'][] = array(
    'type' => 'variable',
    'component' => 'throttle_anonymous',
  );
  $items['form']['throttle_admin_settings']['throttle_user']['#features'][] = array(
    'type' => 'variable',
    'component' => 'throttle_user',
  );
  $items['form']['throttle_admin_settings']['throttle_probability_limiter']['#features'][] = array(
    'type' => 'variable',
    'component' => 'throttle_probability_limiter',
  );

  return $items;
}
