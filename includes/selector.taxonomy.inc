<?php

/**
 * @file
 * A file containing the Drupal UI to Feature's components map for core Taxonomy module.
 */

/**
 * Implements hook_selector_map().
 */
function taxonomy_selector_map() {
  $items = array();

  // Found in taxonomy.admin.inc
  // TODO admin/content/taxonomy
  foreach (taxonomy_get_vocabularies() as $vocabulary) {
    $items['form']['taxonomy_overview_vocabularies'][$vocabulary->vid]['#features'][] = array(
      'type' => 'taxonomy',
      'component' => $vocabulary->name,
    );
  }

  return $items;
}
