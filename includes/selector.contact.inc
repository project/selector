<?php

/**
 * @file
 * A file containing the Drupal UI to Feature's components map for core Contact module.
 */

/**
 * Implements hook_selector_map().
 */
function contact_selector_map() {
  $items = array();

  // Found in contact.admin.inc
  // admin/settings/throttle
  $items['form']['contact_admin_settings']['contact_form_information']['#features'][] = array(
    'type' => 'variable',
    'component' => 'contact_form_information',
  );
  $items['form']['contact_admin_settings']['contact_hourly_threshold']['#features'][] = array(
    'type' => 'variable',
    'component' => 'contact_hourly_threshold',
  );
  $items['form']['contact_admin_settings']['contact_default_status']['#features'][] = array(
    'type' => 'variable',
    'component' => 'contact_default_status',
  );

  return $items;
}
