<?php

/**
 * @file
 * A file containing the Drupal UI to Feature's components map for core Book module.
 */

/**
 * Implements hook_selector_map().
 */
function book_selector_map() {
  $items = array();

  // Found in book.module
  // Implementing Block module.
  // admin/build/block/configure/book/0
  $items['form']['block_admin_configure']['block_settings']['book_block_mode']['#features'][] = array(
    'type' => 'variable',
    'component' => 'book_block_mode',
  );

  // Found in book.admin.inc
  // admin/content/book/settings
  $items['form']['book_admin_settings']['book_allowed_types']['#features'][] = array(
    'type' => 'variable',
    'component' => 'book_allowed_types',
  );
  $items['form']['book_admin_settings']['book_child_type']['#features'][] = array(
    'type' => 'variable',
    'component' => 'book_child_type',
  );

  return $items;
}
