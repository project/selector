<?php

/**
 * @file
 * A file containing the Drupal UI to Feature's components map for Administration menu module.
 */

/**
 * Implements hook_selector_map().
 */
function admin_menu_selector_map() {
  $items = array();

  // Found in admin_menu.inc
  // admin/settings/admin_menu
  $items['form']['admin_menu_theme_settings']['admin_menu_margin_top']['#features'][] = array(
    'type' => 'variable',
    'component' => 'admin_menu_margin_top',
  );
  $items['form']['admin_menu_theme_settings']['admin_menu_position_fixed']['#features'][] = array(
    'type' => 'variable',
    'component' => 'admin_menu_position_fixed',
  );
  $items['form']['admin_menu_theme_settings']['tweaks']['admin_menu_tweak_modules']['#features'][] = array(
    'type' => 'variable',
    'component' => 'admin_menu_tweak_modules',
  );
  $items['form']['admin_menu_theme_settings']['tweaks']['admin_menu_tweak_permissions']['#features'][] = array(
    'type' => 'variable',
    'component' => 'admin_menu_tweak_permissions',
  );
  $items['form']['admin_menu_theme_settings']['tweaks']['admin_menu_tweak_tabs']['#features'][] = array(
    'type' => 'variable',
    'component' => 'admin_menu_tweak_tabs',
  );
  $items['form']['admin_menu_theme_settings']['tweaks']['admin_menu_devel_modules_skip']['#features'][] = array(
    'type' => 'variable',
    'component' => 'admin_menu_devel_modules_skip',
  );

  // Extending Devel module.
  // admin/settings/devel
  $items['form']['devel_admin_settings']['admin_menu']['admin_menu_display']['#features'][] = array(
    'type' => 'variable',
    'component' => 'admin_menu_display',
  );
  $items['form']['devel_admin_settings']['admin_menu']['admin_menu_show_all']['#features'][] = array(
    'type' => 'variable',
    'component' => 'admin_menu_show_all',
  );

  return $items;
}
