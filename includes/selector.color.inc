<?php

/**
 * @file
 * A file containing the Drupal UI to Feature's components map for core Color module.
 */

/**
 * Implements hook_selector_map_alter().
 */
function color_selector_map_alter(&$items) {
  // Found in color.module
  // Extending System module.
  // admin/build/themes/settings/%theme
  foreach (list_themes() as $theme) {
    if (color_get_info($theme->name) && function_exists('gd_info') && variable_get('file_downloads', FILE_DOWNLOADS_PUBLIC) == FILE_DOWNLOADS_PUBLIC) {
      $component = array(
        'type' => 'variable',
        'component' => 'theme_' . $theme->name . '_settings',
      );
      foreach (array_keys($items['form']['system_theme_settings']['theme_settings']['#features'], $component) as $key) {
        unset($items['form']['system_theme_settings']['theme_settings']['#features'][$key]);
      }
      $items['form']['system_theme_settings']['color']['#features'][] = $component;
    }
  }
}
