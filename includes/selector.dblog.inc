<?php

/**
 * @file
 * A file containing the Drupal UI to Feature's components map for core Database logging module.
 */

/**
 * Implements hook_selector_map().
 */
function dblog_selector_map() {
  $items = array();

  // Found in dblog.admin.inc
  // admin/settings/logging/dblog
  $items['form']['dblog_admin_settings']['dblog_row_limit']['#features'][] = array(
    'type' => 'variable',
    'component' => 'dblog_row_limit',
  );

  return $items;
}
