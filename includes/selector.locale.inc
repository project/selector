<?php

/**
 * @file
 * A file containing the Drupal UI to Feature's components map for core Locale module.
 */

/**
 * Implements hook_selector_map().
 */
function locale_selector_map() {
  $items = array();

  // Found in locale.module
  // Extending Node module.
  // admin/content/node-type/%type
  foreach (node_get_types('types', NULL, TRUE) as $type) {
    $type_url_str = str_replace('_', '-', $type->type);
    $items['form']['node_type_form']['workflow']['language_content_type']['#features'][] = array(
      'type' => 'variable',
      'component' => 'language_content_type_' . $type->type,
    );
    $items['page']['admin/content/node-type/' . $type_url_str]['#features'][] = array(
      'type' => 'variable',
      'component' => 'language_content_type_' . $type->type,
    );
  }
  
  // Found in /includes/locale.inc
  // admin/settings/language/configure
  $items['form']['locale_languages_configure_form']['language_negotiation']['#features'][] = array(
    'type' => 'variable',
    'component' => 'language_negotiation',
  );

  return $items;
}
