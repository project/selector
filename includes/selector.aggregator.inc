<?php

/**
 * @file
 * A file containing the Drupal UI to Feature's components map for core Aggregator module.
 */

/**
 * Implements hook_selector_map().
 */
function aggregator_selector_map() {
  $items = array();

  // Found in aggregator.admin.inc
  // admin/content/aggregator/settings
  $items['form']['aggregator_admin_settings']['aggregator_allowed_html_tags']['#features'][] = array(
    'type' => 'variable',
    'component' => 'aggregator_allowed_html_tags',
  );
  $items['form']['aggregator_admin_settings']['aggregator_summary_items']['#features'][] = array(
    'type' => 'variable',
    'component' => 'aggregator_summary_items',
  );
  $items['form']['aggregator_admin_settings']['aggregator_clear']['#features'][] = array(
    'type' => 'variable',
    'component' => 'aggregator_clear',
  );
  $items['form']['aggregator_admin_settings']['aggregator_category_selector']['#features'][] = array(
    'type' => 'variable',
    'component' => 'aggregator_category_selector',
  );

  return $items;
}
