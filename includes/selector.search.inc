<?php

/**
 * @file
 * A file containing the Drupal UI to Feature's components map for core Search module.
 */

/**
 * Implements hook_selector_map().
 */
function search_selector_map() {
  $items = array();

  // Found in search.admin.inc
  // admin/settings/search
  $items['form']['search_admin_settings']['indexing_throttle']['search_cron_limit']['#features'][] = array(
    'type' => 'variable',
    'component' => 'search_cron_limit',
  );
  $items['form']['search_admin_settings']['indexing_settings']['minimum_word_size']['#features'][] = array(
    'type' => 'variable',
    'component' => 'minimum_word_size',
  );
  $items['form']['search_admin_settings']['indexing_settings']['overlap_cjk']['#features'][] = array(
    'type' => 'variable',
    'component' => 'overlap_cjk',
  );

  return $items;
}
