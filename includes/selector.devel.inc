<?php

/**
 * @file
 * A file containing the Drupal UI to Feature's components map for Devel module.
 */

/**
 * Implements hook_selector_map().
 */
function devel_selector_map() {
  $items = array();

  // Found in devel.module
  // Implementing Block module.
  // admin/build/block/configure/devel/0
  $items['form']['block_admin_configure']['block_settings']['list_size']['#features'][] = array(
    'type' => 'variable',
    'component' => 'devel_switch_user_list_size',
  );
  $items['form']['block_admin_configure']['block_settings']['show_form']['#features'][] = array(
    'type' => 'variable',
    'component' => 'devel_switch_user_show_form',
  );

  // admin/settings/devel
  $items['form']['devel_admin_settings']['queries']['dev_query']['#features'][] = array(
    'type' => 'variable',
    'component' => 'dev_query',
  );
  $items['form']['devel_admin_settings']['queries']['devel_query_display']['#features'][] = array(
    'type' => 'variable',
    'component' => 'devel_query_display',
  );
  $items['form']['devel_admin_settings']['queries']['devel_query_sort']['#features'][] = array(
    'type' => 'variable',
    'component' => 'devel_query_sort',
  );
  $items['form']['devel_admin_settings']['queries']['devel_execution']['#features'][] = array(
    'type' => 'variable',
    'component' => 'devel_execution',
  );
  $items['form']['devel_admin_settings']['queries']['devel_store_queries']['#features'][] = array(
    'type' => 'variable',
    'component' => 'devel_store_queries',
  );
  $items['form']['devel_admin_settings']['queries']['devel_store_random']['#features'][] = array(
    'type' => 'variable',
    'component' => 'devel_store_random',
  );
  $items['form']['devel_admin_settings']['xhprof']['devel_xhprof_enabled']['#features'][] = array(
    'type' => 'variable',
    'component' => 'devel_xhprof_enabled',
  );
  $items['form']['devel_admin_settings']['xhprof']['devel_xhprof_directory']['#features'][] = array(
    'type' => 'variable',
    'component' => 'devel_xhprof_directory',
  );
  $items['form']['devel_admin_settings']['xhprof']['devel_xhprof_url']['#features'][] = array(
    'type' => 'variable',
    'component' => 'devel_xhprof_url',
  );
  $items['form']['devel_admin_settings']['devel_api_url']['#features'][] = array(
    'type' => 'variable',
    'component' => 'devel_api_url',
  );
  $items['form']['devel_admin_settings']['dev_timer']['#features'][] = array(
    'type' => 'variable',
    'component' => 'dev_timer',
  );
  $items['form']['devel_admin_settings']['dev_mem']['#features'][] = array(
    'type' => 'variable',
    'component' => 'dev_mem',
  );
  $items['form']['devel_admin_settings']['devel_redirect_page']['#features'][] = array(
    'type' => 'variable',
    'component' => 'devel_redirect_page',
  );
  $items['form']['devel_admin_settings']['devel_error_handler']['#features'][] = array(
    'type' => 'variable',
    'component' => 'devel_error_handler',
  );
  $items['form']['devel_admin_settings']['devel_krumo_skin']['#features'][] = array(
    'type' => 'variable',
    'component' => 'devel_krumo_skin',
  );
  $items['form']['devel_admin_settings']['smtp_library']['#features'][] = array(
    'type' => 'variable',
    'component' => 'smtp_library',
  );
  $items['form']['devel_admin_settings']['devel_rebuild_theme_registry']['#features'][] = array(
    'type' => 'variable',
    'component' => 'devel_rebuild_theme_registry',
  );

  return $items;
}
