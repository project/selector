<?php

/**
 * @file
 * A file containing the Drupal UI to Feature's components map for core Blog API module.
 */

/**
 * Implements hook_selector_map().
 */
function blogapi_selector_map() {
  $items = array();

  // Found in blogapi.module
  // admin/settings/blogapi
  $items['form']['blogapi_admin_settings']['blogapi_node_types']['#features'][] = array(
    'type' => 'variable',
    'component' => 'blogapi_node_types',
  );
  $items['form']['blogapi_admin_settings']['settings_general']['blogapi_extensions_default']['#features'][] = array(
    'type' => 'variable',
    'component' => 'blogapi_extensions_default',
  );
  $items['form']['blogapi_admin_settings']['settings_general']['blogapi_uploadsize_default']['#features'][] = array(
    'type' => 'variable',
    'component' => 'blogapi_uploadsize_default',
  );
  $items['form']['blogapi_admin_settings']['settings_general']['blogapi_usersize_default']['#features'][] = array(
    'type' => 'variable',
    'component' => 'blogapi_usersize_default',
  );
  $roles = user_roles(0, 'administer content with blog api');
  foreach ($roles as $rid => $role) {
    $items['form']['blogapi_admin_settings']['settings_role_' . $rid]['blogapi_extensions_' . $rid]['#features'][] = array(
      'type' => 'variable',
      'component' => 'blogapi_extensions_' . $rid,
    );
    $items['form']['blogapi_admin_settings']['settings_role_' . $rid]['blogapi_uploadsize_' . $rid]['#features'][] = array(
      'type' => 'variable',
      'component' => 'blogapi_uploadsize_' . $rid,
    );
    $items['form']['blogapi_admin_settings']['settings_role_' . $rid]['blogapi_usersize_' . $rid]['#features'][] = array(
      'type' => 'variable',
      'component' => 'blogapi_usersize_' . $rid,
    );
  }

  return $items;
}
