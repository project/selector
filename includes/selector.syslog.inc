<?php

/**
 * @file
 * A file containing the Drupal UI to Feature's components map for core Syslog module.
 */

/**
 * Implements hook_selector_map().
 */
function syslog_selector_map() {
  $items = array();

  // Found in syslog.module
  // admin/settings/logging/syslog
  $items['form']['syslog_admin_settings']['syslog_identity']['#features'][] = array(
    'type' => 'variable',
    'component' => 'syslog_identity',
  );
  $items['form']['syslog_admin_settings']['syslog_facility']['#features'][] = array(
    'type' => 'variable',
    'component' => 'syslog_facility',
  );

  return $items;
}
