<?php

/**
 * @file
 * A file containing the Drupal UI to Feature's components map for core User module.
 */

/**
 * Implements hook_selector_map().
 */
function user_selector_map() {
  $items = array();

  // Found in user.module
  // Implementing Block module.
  // admin/build/block/configure/user/2
  $items['form']['block_admin_configure']['block_settings']['user_block_whois_new_count']['#features'][] = array(
    'type' => 'variable',
    'component' => 'user_block_whois_new_count',
  );
  // admin/build/block/configure/user/3
  $items['form']['block_admin_configure']['block_settings']['user_block_seconds_online']['#features'][] = array(
    'type' => 'variable',
    'component' => 'user_block_seconds_online',
  );
  $items['form']['block_admin_configure']['block_settings']['user_block_max_list_count']['#features'][] = array(
    'type' => 'variable',
    'component' => 'user_block_max_list_count',
  );

  // Found in user.admin.inc
  // admin/user/settings
  $items['form']['user_admin_settings']['registration']['user_register']['#features'][] = array(
    'type' => 'variable',
    'component' => 'user_register',
  );
  $items['form']['user_admin_settings']['registration']['user_email_verification']['#features'][] = array(
    'type' => 'variable',
    'component' => 'user_email_verification',
  );
  $items['form']['user_admin_settings']['registration']['user_registration_help']['#features'][] = array(
    'type' => 'variable',
    'component' => 'user_registration_help',
  );
  $items['form']['user_admin_settings']['email']['admin_created']['user_mail_register_admin_created_subject']['#features'][] = array(
    'type' => 'variable',
    'component' => 'user_mail_register_admin_created_subject',
  );
  $items['form']['user_admin_settings']['email']['admin_created']['user_mail_register_admin_created_body']['#features'][] = array(
    'type' => 'variable',
    'component' => 'user_mail_register_admin_created_body',
  );
  $items['form']['user_admin_settings']['email']['no_approval_required']['user_mail_register_no_approval_required_subject']['#features'][] = array(
    'type' => 'variable',
    'component' => 'user_mail_register_no_approval_required_subject',
  );
  $items['form']['user_admin_settings']['email']['no_approval_required']['user_mail_register_no_approval_required_body']['#features'][] = array(
    'type' => 'variable',
    'component' => 'user_mail_register_no_approval_required_body',
  );
  $items['form']['user_admin_settings']['email']['pending_approval']['user_mail_register_pending_approval_subject']['#features'][] = array(
    'type' => 'variable',
    'component' => 'user_mail_register_pending_approval_subject',
  );
  $items['form']['user_admin_settings']['email']['pending_approval']['user_mail_register_pending_approval_body']['#features'][] = array(
    'type' => 'variable',
    'component' => 'user_mail_register_pending_approval_body',
  );
  $items['form']['user_admin_settings']['email']['password_reset']['user_mail_password_reset_subject']['#features'][] = array(
    'type' => 'variable',
    'component' => 'user_mail_password_reset_subject',
  );
  $items['form']['user_admin_settings']['email']['password_reset']['user_mail_password_reset_body']['#features'][] = array(
    'type' => 'variable',
    'component' => 'user_mail_password_reset_body',
  );
  $items['form']['user_admin_settings']['email']['activated']['user_mail_status_activated_notify']['#features'][] = array(
    'type' => 'variable',
    'component' => 'user_mail_status_activated_notify',
  );
  $items['form']['user_admin_settings']['email']['activated']['user_mail_status_activated_subject']['#features'][] = array(
    'type' => 'variable',
    'component' => 'user_mail_status_activated_subject',
  );
  $items['form']['user_admin_settings']['email']['activated']['user_mail_status_activated_body']['#features'][] = array(
    'type' => 'variable',
    'component' => 'user_mail_status_activated_body',
  );
  $items['form']['user_admin_settings']['email']['blocked']['user_mail_status_blocked_notify']['#features'][] = array(
    'type' => 'variable',
    'component' => 'user_mail_status_blocked_notify',
  );
  $items['form']['user_admin_settings']['email']['blocked']['user_mail_status_blocked_subject']['#features'][] = array(
    'type' => 'variable',
    'component' => 'user_mail_status_blocked_subject',
  );
  $items['form']['user_admin_settings']['email']['blocked']['user_mail_status_blocked_body']['#features'][] = array(
    'type' => 'variable',
    'component' => 'user_mail_status_blocked_body',
  );
  $items['form']['user_admin_settings']['email']['deleted']['user_mail_status_deleted_notify']['#features'][] = array(
    'type' => 'variable',
    'component' => 'user_mail_status_deleted_notify',
  );
  $items['form']['user_admin_settings']['email']['deleted']['user_mail_status_deleted_subject']['#features'][] = array(
    'type' => 'variable',
    'component' => 'user_mail_status_deleted_subject',
  );
  $items['form']['user_admin_settings']['email']['deleted']['user_mail_status_deleted_body']['#features'][] = array(
    'type' => 'variable',
    'component' => 'user_mail_status_deleted_body',
  );
  $items['form']['user_admin_settings']['signatures']['user_signatures']['#features'][] = array(
    'type' => 'variable',
    'component' => 'user_signatures',
  );
  $items['form']['user_admin_settings']['pictures']['user_pictures']['#features'][] = array(
    'type' => 'variable',
    'component' => 'user_pictures',
  );
  $items['form']['user_admin_settings']['pictures']['settings']['user_picture_path']['#features'][] = array(
    'type' => 'variable',
    'component' => 'user_picture_path',
  );
  $items['form']['user_admin_settings']['pictures']['settings']['user_picture_default']['#features'][] = array(
    'type' => 'variable',
    'component' => 'user_picture_default',
  );
  $items['form']['user_admin_settings']['pictures']['settings']['user_picture_dimensions']['#features'][] = array(
    'type' => 'variable',
    'component' => 'user_picture_dimensions',
  );
  $items['form']['user_admin_settings']['pictures']['settings']['user_picture_file_size']['#features'][] = array(
    'type' => 'variable',
    'component' => 'user_picture_file_size',
  );
  $items['form']['user_admin_settings']['pictures']['settings']['user_picture_guidelines']['#features'][] = array(
    'type' => 'variable',
    'component' => 'user_picture_guidelines',
  );

  return $items;
}
