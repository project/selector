<?php

/**
 * @file
 * A file containing the Drupal UI to Feature's components map for core Upload module.
 */

/**
 * Implements hook_selector_map().
 */
function upload_selector_map() {
  $items = array();

  // Found in upload.module
  // Extending Node module.
  // admin/content/node-type/%type
  foreach (node_get_types('types', NULL, TRUE) as $type) {
    $type_url_str = str_replace('_', '-', $type->type);
    $items['form']['node_type_form']['workflow']['upload']['#features'][] = array(
      'type' => 'variable',
      'component' => 'upload_' . $type->type,
    );
    $items['page']['admin/content/node-type/' . $type_url_str]['#features'][] = array(
      'type' => 'variable',
      'component' => 'upload_' . $type->type,
    );
  }

  // Found in upload.admin.inc
  // admin/settings/uploads
  $items['form']['upload_admin_settings']['settings_general']['upload_max_resolution']['#features'][] = array(
    'type' => 'variable',
    'component' => 'upload_max_resolution',
  );
  $items['form']['upload_admin_settings']['settings_general']['upload_list_default']['#features'][] = array(
    'type' => 'variable',
    'component' => 'upload_list_default',
  );
  $items['form']['upload_admin_settings']['settings_general']['upload_extensions_default']['#features'][] = array(
    'type' => 'variable',
    'component' => 'upload_extensions_default',
  );
  $items['form']['upload_admin_settings']['settings_general']['upload_uploadsize_default']['#features'][] = array(
    'type' => 'variable',
    'component' => 'upload_uploadsize_default',
  );
  $items['form']['upload_admin_settings']['settings_general']['upload_usersize_default']['#features'][] = array(
    'type' => 'variable',
    'component' => 'upload_usersize_default',
  );
  $roles = user_roles(FALSE, 'upload files');
  foreach ($roles as $rid => $role) {
    $items['form']['upload_admin_settings']['settings_role_' . $rid]['upload_extensions_' . $rid]['#features'][] = array(
      'type' => 'variable',
      'component' => 'upload_extensions_' . $rid,
    );
    $items['form']['upload_admin_settings']['settings_role_' . $rid]['upload_uploadsize_' . $rid]['#features'][] = array(
      'type' => 'variable',
      'component' => 'upload_uploadsize_' . $rid,
    );
    $items['form']['upload_admin_settings']['settings_role_' . $rid]['upload_usersize_' . $rid]['#features'][] = array(
      'type' => 'variable',
      'component' => 'upload_usersize_' . $rid,
    );
  }

  return $items;
}
