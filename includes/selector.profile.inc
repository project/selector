<?php

/**
 * @file
 * A file containing the Drupal UI to Feature's components map for core Profile module.
 */

/**
 * Implements hook_selector_map().
 */
function profile_selector_map() {
  $items = array();

  // Found in profile.module
  // Implementing Block module.
  // admin/build/block/configure/profile/0
  $items['form']['block_admin_configure']['block_settings']['profile_block_author_fields']['#features'][] = array(
    'type' => 'variable',
    'component' => 'profile_block_author_fields',
  );

  return $items;
}
