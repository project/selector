<?php

/**
 * @file
 * A file containing the Drupal UI to Feature's components map for core Node module.
 */

/**
 * Implements hook_selector_map().
 */
function node_selector_map() {
  $items = array();

  // Found in node.module
  // Implementing Search module.
  // admin/settings/search
  $items['form']['search_admin_settings']['content_ranking']['factors']['node_rank_relevance']['#features'][] = array(
    'type' => 'variable',
    'component' => 'node_rank_relevance',
  );
  $items['form']['search_admin_settings']['content_ranking']['factors']['node_rank_recent']['#features'][] = array(
    'type' => 'variable',
    'component' => 'node_rank_recent',
  );
  $items['form']['search_admin_settings']['content_ranking']['factors']['node_rank_comments']['#features'][] = array(
    'type' => 'variable',
    'component' => 'node_rank_comments',
  );
  $items['form']['search_admin_settings']['content_ranking']['factors']['node_rank_views']['#features'][] = array(
    'type' => 'variable',
    'component' => 'node_rank_views',
  );

  // Found in content_types.inc
  // admin/content/node-type/%type
  foreach (node_get_types('types', NULL, TRUE) as $type) {
    $type_url_str = str_replace('_', '-', $type->type);
    $items['form']['node_type_form']['workflow']['node_options']['#features'][] = array(
      'type' => 'variable',
      'component' => 'node_options_' . $type->type,
    );
    $items['page']['admin/content/node-type/' . $type_url_str]['#features'][] = array(
      'type' => 'variable',
      'component' => 'node_options_' . $type->type,
    );
  }

  // Found in node.admin.inc
  // admin/content/node-settings
  $items['form']['node_configure']['default_nodes_main']['#features'][] = array(
    'type' => 'variable',
    'component' => 'default_nodes_main',
  );
  $items['form']['node_configure']['teaser_length']['#features'][] = array(
    'type' => 'variable',
    'component' => 'teaser_length',
  );
  $items['form']['node_configure']['node_preview']['#features'][] = array(
    'type' => 'variable',
    'component' => 'node_preview',
  );

  return $items;
}
