
Drupal.behaviors.selector = function() {
  // Select a feature to include a component or cancel a selection.
  $('.selector-widget').each(function() {
    var widget = $(this);
    // Click restrictions to notselected and conflict features only.
    $(this).find('.selector-conflict, .selector-notselected, .selector-cancel').each(function() {
      var selection = $(this);
      $(this).find('a').click(function() {
        var uri = $(this).attr('href');
        $.get(uri, [], function(data) {
          // Statuses of the features buttons.
          for (feature in data.features) {
            $(widget).find('.selector-feature-' + feature).removeClass('selector-selected selector-included selector-conflict selector-notselected').addClass('selector-' + data.features[feature]);
            if (data.features[feature] == 'selected') {
              var selected = true;
            }
          };
          // Status of the cancel button.
          if (selected) {
            $(widget).find('.selector-cancel, .selector-disabled').removeClass('selector-cancel selector-disabled').addClass('selector-cancel');
          }
          else {
            $(widget).find('.selector-cancel, .selector-disabled').removeClass('selector-cancel selector-disabled').addClass('selector-disabled');
          }
        }, 'json');
        return false;
      });
    });
    // Default action for other buttons.
    $(this).find('.selector-selected, .selector-included, .selector-disabled').each(function() {
      $(this).find('a').click(function() {
        return false;
      });
    });
  });
};
