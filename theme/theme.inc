<?php

/**
 * @file
 * A file containing theme functions for the Selector module.
 */

/**
 * Theme a set of selected components in integration with Features.
 */
function theme_selector_features_components($info, $selected = array()) {
  $output = '';
  $rows = array();
  $included = features_get_components();

  if (!empty($selected)) {
    foreach ($selected as $type => $components) {
      $items = array();
      foreach ($components as $component) {
        // Include only if not included yet.
        if (isset($info['features'][$type]) && in_array($component, $info['features'][$type])) {
          break;
        }
        $items[] = $component;
      }

      if (!empty($items)) {
        $rows[] = array(array(
          'data' => isset($included[$type]['name']) ? $included[$type]['name'] : $type,
          'header' => TRUE,
        ));
        $rows[] = array(array(
          'data' => theme('selector_features_component_list', $items),
          'class' => 'component',
        ));
      }
    }

    $output .= theme('table', array(), $rows);
    $output .= theme('selector_features_component_key');
  }

  return $output;
}

/**
 * Theme selected components in a component list in integration with Features.
 */
function theme_selector_features_component_list($components) {
  $list = array();
  foreach ($components as $component) {
    $list[] = "<span class='features-selected'>". check_plain($component) ."</span>";
  }
  return "<span class='features-component-list'>". implode(' ', $list) ."</span>";
}

/**
 * Provide an additional themed key for a component list in integration with Features.
 */
function theme_selector_features_component_key() {
  $list = array();
  $list[] = "<span class='features-selected'>" . t('Selected') . "</span>";
  return "<span class='features-component-list features-component-key'>". implode(' ', $list) ."</span>";
}

/**
 * Theme a widget's link.
 */
function theme_selector_link($feature = NULL, $type, $component, $status) {
  switch ($status) {
    case SELECTOR_SELECTED:
      $link = l(check_plain($feature), "admin/build/selector/$feature/select/$type/$component", array('fragment' => "selector-component-$type-$component", 'query' => array('destination' => drupal_get_destination()), 'attributes' => array('title' => t('Selected'))));
      return "<span class='selector-selected selector-feature-$feature'>{$link}</span>";
    case SELECTOR_INCLUDED:
      $link = l(check_plain($feature), "admin/build/selector/$feature/select/$type/$component", array('fragment' => "selector-component-$type-$component", 'query' => array('destination' => drupal_get_destination()), 'attributes' => array('title' => t('Included'))));
      return "<span class='selector-included selector-feature-$feature'>{$link}</span>";
    case SELECTOR_CONFLICT:
      $link = l(check_plain($feature), "admin/build/selector/$feature/select/$type/$component", array('fragment' => "selector-component-$type-$component", 'query' => array('destination' => drupal_get_destination()), 'attributes' => array('title' => t('Conflict'))));
      return "<span class='selector-conflict selector-feature-$feature'>{$link}</span>";
    case SELECTOR_NOTSELECTED:
      $link = l(check_plain($feature), "admin/build/selector/$feature/select/$type/$component", array('fragment' => "selector-component-$type-$component", 'query' => array('destination' => drupal_get_destination()), 'attributes' => array('title' => t('Not selected'))));
      return "<span class='selector-notselected selector-feature-$feature'>{$link}</span>";
    case SELECTOR_CANCEL:
      $link = l(t('Cancel'), "admin/build/selector/cancel/$type/$component", array('fragment' => "selector-component-$type-$component", 'query' => array('destination' => drupal_get_destination()), 'attributes' => array('title' => t('Cancel selection'))));
      return "<span class='selector-cancel'>{$link}</span>";
    case SELECTOR_DISABLED:
      $link = l(t('Cancel'), "admin/build/selector/cancel/$type/$component", array('fragment' => "selector-component-$type-$component", 'query' => array('destination' => drupal_get_destination()), 'attributes' => array('title' => t('Cancel selection'))));
      return "<span class='selector-disabled'>{$link}</span>";
  }
}

/**
 * Theme the widget.
 */
function theme_selector_widget($type, $component, $links) {
  return '<div class="selector-widget ' . "selector-component-$type-$component" . '"><a name="' . "selector-component-$type-$component" . '">' . "$type: $component" . '</a>: ' . implode(' ', $links) . '</div>';
}
